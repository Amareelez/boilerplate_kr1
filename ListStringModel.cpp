//
// Created by amareelez on 10/4/19.
//

#include "ListStringModel.h"

#include <memory>
#include "ListStringIterator.h"
#include "ListStringVisitor.h"

void ListStringModel::push_back(std::string s) {
  if (size_ == capacity_) {
    reallocateStorage();
  }
  storage_[size_++] = std::move(s);
  if (handler_) {
    handler_->operator()();
  }
}

void ListStringModel::reallocateStorage() {
  auto new_storage = new std::string[2 * capacity_ + 1];
  for (size_t i = 0; i < size_; ++i) {
    new_storage[i] = storage_[i];
  }
  delete[] storage_;
  storage_ = new_storage;
  capacity_ = 2 * capacity_ + 1;
}

void ListStringModel::pop_back() {
  storage_[size_ - 1].~basic_string();
  --size_;
  if (handler_) {
    handler_->operator()();
  }
}

void ListStringModel::pop_front() {
  storage_[0].~basic_string();
  for (size_t i = 0; i + 1 < size_; ++i) {
    storage_[i] = storage_[i + 1];
  }
  --size_;
  if (handler_) {
    handler_->operator()();
  }
}

std::unique_ptr<ListStringIterator> ListStringModel::createIterator() const {
  return std::make_unique<ListStringIterator>(*this);
}

std::string ListStringModel::min() const {
  auto visitor = MinListStringVisitor();
  auto iterator = *createIterator();
  while (iterator.hasNext()) {
    iterator.accept(visitor);
    iterator.advance();
  }
  return visitor.result();
}

std::string ListStringModel::max() const {
  auto visitor = MaxListStringVisitor();
  auto iterator = *createIterator();
  while (iterator.hasNext()) {
    iterator.accept(visitor);
    iterator.advance();
  }
  return visitor.result();
}

ListStringModel::~ListStringModel() {
  for (size_t i = 0; i < size_; ++i) {
    storage_[i].~basic_string();
  }
  delete[] storage_;
}
