//
// Created by amareelez on 10/4/19.
//

#include "ListStringVisitor.h"

void MinListStringVisitor::visit(const std::string& s) {
  if (!once_set || s.size() < min_.size()) {
    min_ = s;
    once_set = true;
  }
}

void MaxListStringVisitor::visit(const std::string& s) {
  if (s.size() >= max_.size()) {
    max_ = s;
  }
}
