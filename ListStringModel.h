//
// Created by amareelez on 10/4/19.
//

#ifndef MVC_BOILERPLATE__LISTSTRINGMODEL_H_
#define MVC_BOILERPLATE__LISTSTRINGMODEL_H_

#include <memory>
#include <functional>

class ListStringIterator;

class ListStringModel {
 public:
  void push_back(std::string s);

  void pop_back();

  void pop_front();

  std::string at(size_t i) const {
    return storage_[i];
  }

  size_t size() const {
    return size_;
  }

  size_t capacity() const {
    return capacity_;
  }

  std::unique_ptr<ListStringIterator> createIterator() const;

  std::string min() const;

  std::string max() const;

  void setDataChangeHandler(std::shared_ptr<const std::function<void()>> handler) {
    handler_ = std::move(handler);
  }

  ~ListStringModel();
 private:
  void reallocateStorage();

  std::string* storage_ = nullptr;
  size_t size_ = 0;
  size_t capacity_ = 0;
  std::shared_ptr<const std::function<void()>> handler_ = nullptr;
};

#endif //MVC_BOILERPLATE__LISTSTRINGMODEL_H_
