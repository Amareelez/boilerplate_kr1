#include <iostream>
#include <cassert>

#include "ListStringModel.h"
#include "ListStringIterator.h"
#include "ListStringVisitor.h"
#include "ListStringController.h"
#include "ListStringView.h"

void testReallocation() {
  auto model = ListStringModel();
  assert(model.size() == 0);
  assert(model.capacity() == 0);

  model.push_back("");
  assert(model.size() == 1);
  assert(model.capacity() == 1);

  model.push_back("");
  assert(model.size() == 2);
  assert(model.capacity() == 3);

  for (size_t i = 0; i < 6; ++i) {
    model.push_back("");
  }
  assert(model.size() == 8);
  assert(model.capacity() == 15);
}

void testPushBack() {
  auto model = ListStringModel();
  model.push_back("a");
  assert(model.at(0) == "a");

  model.push_back("b");
  assert(model.at(0) == "a");
  assert(model.at(1) == "b");

  for (size_t i = 0; i < 100; ++i) {
    model.push_back(std::to_string(i));
    assert(model.at(model.size() - 1) == std::to_string(i));
  }
  assert(model.size() == 102);
}

void testPopBack() {
  auto model = ListStringModel();
  model.push_back("a");
  model.pop_back();
  assert(model.size() == 0);

  for (size_t i = 0; i < 10; ++i) {
    model.push_back(std::to_string(i));
  }

  for (size_t i = 0; i < 10; ++i) {
    assert(model.at(model.size() - 1) == std::to_string(9 - i));
    model.pop_back();
  }
  assert(model.size() == 0);
}

void testPopFront() {
  auto model = ListStringModel();
  model.push_back("a");
  model.pop_front();
  assert(model.size() == 0);

  for (size_t i = 0; i < 10; ++i) {
    model.push_back(std::to_string(i));
  }

  for (size_t i = 0; i < 10; ++i) {
    assert(model.at(0) == std::to_string(i));
    model.pop_front();
  }
  assert(model.size() == 0);
}

void testEmptyIterator() {
  auto model = ListStringModel();
  auto iterator = *model.createIterator();
  assert(iterator.hasNext() == false);
}

void testIterator() {
  auto model = ListStringModel();
  for (size_t i = 0; i < 10; ++i) {
    model.push_back(std::to_string(i));
  }

  auto iterator = *model.createIterator();
  for (size_t i = 0; i < 10; ++i) {
    assert(iterator.hasNext() == true);
    assert(iterator.currentItem() == std::to_string(i));
    iterator.advance();
  }
  assert(iterator.hasNext() == false);
}

void testMinVisitor() {
  auto model = ListStringModel();
  for (size_t i = 0; i < 20; ++i) {
    model.push_back(std::to_string(i));
  }

  auto visitor = MinListStringVisitor();
  auto iterator = *model.createIterator();
  while (iterator.hasNext()) {
    iterator.accept(visitor);
    iterator.advance();
  }
  assert(visitor.result() == "0");
}

void testMaxVisitor() {
  auto model = ListStringModel();
  for (size_t i = 0; i < 20; ++i) {
    model.push_back(std::to_string(i));
  }

  auto visitor = MaxListStringVisitor();
  auto iterator = *model.createIterator();
  while (iterator.hasNext()) {
    iterator.accept(visitor);
    iterator.advance();
  }
  assert(visitor.result() == "19");
}

void testController() {
  auto model_ptr = std::make_shared<ListStringModel>();
  auto controller = ListStringController(model_ptr, nullptr);
  for (size_t i = 0; i < 20; ++i) {
    controller.push_back(std::to_string(i));
  }
  assert(model_ptr->size() == 20);
  assert(model_ptr->at(0) == "0");
  assert(model_ptr->at(model_ptr->size() - 1) == "19");

  assert(controller.min() == "0");
  assert(controller.max() == "19");

  controller.pop_back();
  assert(model_ptr->size() == 19);
  assert(model_ptr->at(model_ptr->size() - 1) == "18");
  assert(controller.max() == "18");

  controller.pop_front();
  assert(model_ptr->size() == 18);
  assert(model_ptr->at(0) == "1");
  assert(controller.min() == "1");
}

void testMinMaxView() {
  auto model_ptr = std::make_shared<ListStringModel>();
  model_ptr->push_back("0");
  auto view_ptr = std::unique_ptr<BaseListStringView>(new MinMaxListStringView(model_ptr));
  auto controller = ListStringController(model_ptr, std::move(view_ptr));

  for (size_t i = 1; i < 10; ++i) {
    controller.push_back(std::to_string(i));
  }

  controller.pop_back();
  controller.pop_front();
}

void testVerticalListView() {
  auto model_ptr = std::make_shared<ListStringModel>();
  model_ptr->push_back("0");
  auto view_ptr = std::unique_ptr<BaseListStringView>(new VerticalListListStringView(model_ptr));
  auto controller = ListStringController(model_ptr, std::move(view_ptr));

  for (size_t i = 1; i < 10; ++i) {
    controller.push_back(std::to_string(i));
  }

  controller.pop_back();
  controller.pop_front();
}

int main() {
  testReallocation();
  testPushBack();
  testPopBack();
  testPopFront();
  testEmptyIterator();
  testIterator();
  testMinVisitor();
  testMaxVisitor();
  testController();
  testMinMaxView();
  testVerticalListView();
  return 0;
}