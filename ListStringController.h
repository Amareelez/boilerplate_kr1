//
// Created by amareelez on 10/4/19.
//

#ifndef MVC_BOILERPLATE__LISTSTRINGCONTROLLER_H_
#define MVC_BOILERPLATE__LISTSTRINGCONTROLLER_H_

#include "ListStringModel.h"
#include "ListStringView.h"

#include <string>

class ListStringController {
 public:
  ListStringController(
      std::shared_ptr<ListStringModel> model,
      std::unique_ptr<BaseListStringView> view)
      : model_(std::move(model)),
        view_(std::move(view)) {
    if (view_) {
      view_->render();
      model_->setDataChangeHandler(view_->handler());
    }
  }

  void push_back(std::string s);

  void pop_back();

  void pop_front();

  std::string min() const;

  std::string max() const;
 private:
  std::shared_ptr<ListStringModel> model_;
  std::unique_ptr<BaseListStringView> view_;
};

#endif //MVC_BOILERPLATE__LISTSTRINGCONTROLLER_H_
