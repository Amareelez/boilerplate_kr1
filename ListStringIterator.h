//
// Created by amareelez on 10/4/19.
//

#ifndef MVC_BOILERPLATE__LISTSTRINGITERATOR_H_
#define MVC_BOILERPLATE__LISTSTRINGITERATOR_H_

#include "ListStringModel.h"
#include "ListStringVisitor.h"

class ListStringIterator {
 public:
  explicit ListStringIterator(const ListStringModel& model) : model_(model), index_(0) {}

  bool hasNext() const {
    return index_ < model_.size();
  }

  std::string currentItem() const {
    return model_.at(index_);
  }

  void advance() {
    ++index_;
  }

  void accept(BaseListStringVisitor& visitor) {
    visitor.visit(currentItem());
  }
 private:
  const ListStringModel& model_;
  size_t index_;
};

#endif //MVC_BOILERPLATE__LISTSTRINGITERATOR_H_
