//
// Created by amareelez on 10/4/19.
//

#ifndef MVC_BOILERPLATE__LISTSTRINGVIEW_H_
#define MVC_BOILERPLATE__LISTSTRINGVIEW_H_

#include "ListStringModel.h"

#include <functional>

class BaseListStringView {
 public:
  explicit BaseListStringView(std::shared_ptr<ListStringModel> model) : model_(std::move(model)) {}
  virtual void render() const = 0;
  std::shared_ptr<const std::function<void()>> handler() const {
    return handler_;
  }
 protected:
  std::shared_ptr<ListStringModel> model_;
  std::shared_ptr<const std::function<void()>> handler_ =
      std::make_shared<const std::function<void()>>([this]() {
        render();
      });
};

class MinMaxListStringView : public BaseListStringView {
 public:
  explicit MinMaxListStringView(std::shared_ptr<ListStringModel> model)
      : BaseListStringView(std::move(model)) {}
  void render() const override;
};

class VerticalListListStringView : public BaseListStringView {
 public:
  explicit VerticalListListStringView(std::shared_ptr<ListStringModel> model)
      : BaseListStringView(std::move(model)) {}
  void render() const override;
};

#endif //MVC_BOILERPLATE__LISTSTRINGVIEW_H_
