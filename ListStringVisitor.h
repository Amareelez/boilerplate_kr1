//
// Created by amareelez on 10/4/19.
//

#ifndef MVC_BOILERPLATE__LISTSTRINGVISITOR_H_
#define MVC_BOILERPLATE__LISTSTRINGVISITOR_H_

#include <string>

class BaseListStringVisitor {
 public:
  virtual void visit(const std::string& s) = 0;
  virtual std::string result() const = 0;
};

class MinListStringVisitor : public BaseListStringVisitor {
 public:
  void visit(const std::string& s) override;

  std::string result() const override {
    return min_;
  }
 private:
  std::string min_;
  bool once_set = false;
};

class MaxListStringVisitor : public BaseListStringVisitor {
 public:
  void visit(const std::string& s) override;

  std::string result() const override {
    return max_;
  }
 private:
  std::string max_;
};

#endif //MVC_BOILERPLATE__LISTSTRINGVISITOR_H_
