//
// Created by amareelez on 10/4/19.
//

#include "ListStringController.h"

void ListStringController::push_back(std::string s) {
  model_->push_back(std::move(s));
}

void ListStringController::pop_back() {
  model_->pop_back();
}

void ListStringController::pop_front() {
  model_->pop_front();
}

std::string ListStringController::min() const {
  return model_->min();
}

std::string ListStringController::max() const {
  return model_->max();
}
