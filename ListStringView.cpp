//
// Created by amareelez on 10/4/19.
//

#include "ListStringView.h"
#include "ListStringIterator.h"

#include <iostream>

void MinMaxListStringView::render() const {
  std::cout << "[Min = " << model_->min() << ", Max = " << model_->max() << "]\n";
}

void VerticalListListStringView::render() const {
  auto iterator = *model_->createIterator();
  std::cout << "[";
  while (iterator.hasNext()) {
    std::cout << "\n\t" << iterator.currentItem();
    iterator.advance();
  }
  std::cout << "\n]\n";
}
